import os
import sys
import subprocess
import logging

logging.basicConfig(level=logging.INFO)

tools_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'tools')

sys.path.append(os.path.join(tools_path, 'bait-launchers'))

# initialize environment
environment = {}

environment["FTRACK_CONNECT_PLUGIN_PATH"] = [os.path.join(tools_path,
                                                          "ftrack",
                                                          "ftrack-hooks")]

# setup environment
for variable in environment:
    for path in environment[variable]:
        try:
            os.environ[variable] += os.pathsep + path
        except:
            os.environ.setdefault(variable, path)

# Launching FTrack-connect
path = os.path.join(tools_path, 'ftrack', 'ftrack-connect-package',
                    'windows', 'current', 'ftrack_connect_package.exe')
subprocess.Popen(path)
